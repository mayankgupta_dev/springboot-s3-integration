package com.s3.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

@Service
public class S3Service {
	
	@Autowired
	private AmazonS3 s3Client;
	
	String bucketName = "demo-mayank-s3";
	
	public String uploadImage(MultipartFile file, String stringObjKeyName) {
		// PutObjectResult result = s3Client.putObject(bucketName, stringObjKeyName,
		// fileName);
		try {
		//	byte[] byteArr = file.getBytes();
			// Upload a file as a new object with ContentType and title specified.
			
			
			
//			PutObjectRequest request = new PutObjectRequest(bucketName, stringObjKeyName,
//					new File(file.getOriginalFilename()));
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType("image/png");
			metadata.addUserMetadata("title", "Image upload");
		//	request.setMetadata(metadata);
			s3Client.putObject(bucketName, stringObjKeyName, file.getInputStream(), metadata);
		//	s3Client.putObject(request);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			file.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}

		

		return "File Uploaded Successfully";
	}

	
	public String deleteSingleObject(String keyName) {
		s3Client.deleteObject(bucketName, keyName);
		return "Object Deleted Sccessfully";
		
	}
	
	
	public String downloadAnObject(String keyName) {

		ResponseHeaderOverrides headersOverrides = new ResponseHeaderOverrides().withCacheControl("No-cache")
				.withContentDisposition("attachment; filename=img.png");

		GetObjectRequest objReq = new GetObjectRequest(bucketName, keyName).withResponseHeaders(headersOverrides);

		try {
			downloadImage(s3Client.getObject(objReq).getObjectContent(), keyName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "File Downloaded";

	}
	
	public String downloadFile(String keyName) {
		S3Object o = s3Client.getObject(bucketName, keyName);
		S3ObjectInputStream s3is = o.getObjectContent();
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(new File(keyName));
			byte[] read_buf = new byte[1024];
			int read_len = 0;
			while ((read_len = s3is.read(read_buf)) > 0) {
				fos.write(read_buf, 0, read_len);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				s3is.close();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return keyName;

	}
	
	private void downloadImage(InputStream input, String keyName) throws IOException {
		BufferedImage image = ImageIO.read(input);
		String imgUrl = createImage(keyName);
		File newfile = new File(imgUrl);
		ImageIO.write(image, "png", newfile);
	}
	
	
	private String createImage(String keyName) {
		String imgUrl = "./image/" + keyName + ".png";

		Path textFilePath = Paths.get(imgUrl);
		try {
			Files.createFile(textFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return imgUrl;
	}
}
