package com.s3.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Configuration
public class AWSConfig {

	/**
	 * 
	 */
	@Bean
	public AmazonS3 getAmzonS3(){
		
		return AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).build();
		
	}
	
}
