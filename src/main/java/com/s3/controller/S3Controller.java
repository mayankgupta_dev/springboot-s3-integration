package com.s3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.s3.service.S3Service;

@RestController
@RequestMapping("/api")
public class S3Controller {
	
	@Autowired
	private S3Service s3Service;

	@PostMapping("/upload")
	public String uploadImage(@RequestParam("img") MultipartFile file, @RequestParam("keyName") String keyName) {
		return s3Service.uploadImage(file, keyName);
	}
	
	@DeleteMapping("/file")
	public String deleteSingleObject(@RequestParam("keyName") String keyName) {
		return s3Service.deleteSingleObject(keyName);
	}
	
	@GetMapping("/file")
	public String downloadAnObject(@RequestParam("keyName") String keyName) {
		return s3Service.downloadAnObject(keyName);
	}
	
}
